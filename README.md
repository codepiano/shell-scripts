## 一些工具脚本

具体说明见各自目录内的README.md

### pull-all-git

在给定目录的所有子文件夹下执行git pull origin master

用来更新目录下的所有git项目

### afar

抓取chrome插件[远方 New Tab](https://chrome.google.com/webstore/detail/dream-afar-new-tab/henmfoppjjkcencpbjaigfahdjlgpegn?hl=zh-CN) 的背景图片的脚本

图片很不错，不过和speed dial类的插件不能共存，写了个脚本把图片抓下来当壁纸随机切换
